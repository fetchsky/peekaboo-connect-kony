package com.fetchsky.peekabookonywrapper;

import android.os.Bundle;
import android.util.Log;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class Utils {
    public static Bundle getBundleFromHashTable(Hashtable params, Bundle bundle) {
        String key;
        Object value;
        Set<String> keys = params.keySet();
        Iterator<String> itr = keys.iterator();
        Log.i("PEEKABOO", params.toString());

        while (itr.hasNext()) {
            key = itr.next();
            value = params.get(key);

            if(value instanceof Boolean) {
                bundle.putBoolean(key, (Boolean) value);
            } else {
                bundle.putString(key, value.toString());
            }
        }

        return bundle;
    }

    public static Bundle getBundleFromHashTable(Hashtable params) {
        return getBundleFromHashTable(params, new Bundle());
    }
}
