package com.fetchsky.peekabookonywrapper;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import com.konylabs.android.KonyApplication;
import com.konylabs.android.KonyMain;

import java.util.Hashtable;
import java.util.List;

public class Wrapper extends KonyApplication implements ReactApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(getAppContext(), false);
    }

    private final ReactNativeHost mReactNativeHost =
      new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
          return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
          @SuppressWarnings("UnnecessaryLocalVariable")
          List<ReactPackage> packages = new PackageList(this).getPackages();
          // Packages that cannot be autolinked yet can be added manually here, for example:
          // packages.add(new ExtraDimensionsPackage());
          return packages;
        }

        @Override
        protected String getJSMainModuleName() {
          return "index";
        }
      };
    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }
}
