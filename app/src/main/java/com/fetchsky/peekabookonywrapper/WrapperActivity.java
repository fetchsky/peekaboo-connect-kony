package com.fetchsky.peekabookonywrapper;

import android.content.Intent;
import android.os.Bundle;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
//import org.devio.rn.splashscreen.SplashScreen;

public class WrapperActivity extends ReactActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
//        SplashScreen.show(this);
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "peekaboosdk";
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected Bundle getLaunchOptions() {
                Intent intent = getIntent();
                Bundle initialProps = new Bundle();
                initialProps.putString("default_key", "DEFAULT");
                if (intent.getExtras() != null) {
                    initialProps = intent.getExtras();
                }
                return initialProps;
            }
        };
    }
}
